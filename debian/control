Source: libwww-mechanize-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 11)
Build-Depends-Indep: perl,
                     libcgi-pm-perl (>= 4.32),
                     libhtml-form-perl,
                     libhtml-parser-perl,
                     libhtml-tree-perl,
                     libhttp-cookies-perl,
                     libhttp-daemon-perl,
                     libhttp-message-perl,
                     libhttp-server-simple-perl,
                     libio-socket-ssl-perl,
                     libtest-deep-perl,
                     libtest-exception-perl,
                     libtest-fatal-perl,
                     libtest-memory-cycle-perl,
                     libtest-nowarnings-perl,
                     libtest-output-perl,
                     libtest-pod-coverage-perl,
                     libtest-pod-perl,
                     libtest-taint-perl,
                     libtest-warn-perl,
                     libtest-warnings-perl,
                     liburi-perl,
                     libwww-perl,
                     netbase
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libwww-mechanize-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libwww-mechanize-perl.git
Homepage: https://metacpan.org/release/WWW-Mechanize

Package: libwww-mechanize-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libcgi-pm-perl (>= 4.32),
         libhtml-form-perl,
         libhtml-parser-perl,
         libhtml-tree-perl,
         libhttp-cookies-perl,
         libhttp-daemon-perl,
         libhttp-message-perl,
         libhttp-server-simple-perl,
         liburi-perl,
         libwww-perl
Recommends: libio-socket-ssl-perl
Description: module to automate interaction with websites
 WWW::Mechanize, or Mech for short, helps you automate interaction with
 a website. It supports performing a sequence of page fetches including
 following links and submitting forms. Each fetched page is parsed and
 its links and forms are extracted. A link or a form can be selected, form
 fields can be filled and the next page can be fetched. Mech also stores
 a history of the URLs you've visited, which can be queried and revisited.
